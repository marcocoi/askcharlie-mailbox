# README #

* askCharlie mailbox.
* Author: Marco Nascimento

### About ###
Responsive layout mailbox component built with AngularJS.

### Working URL ###
https://askcharlie-mailbox.herokuapp.com/

**Mock data url:** http://www.mocky.io/v2/57b0ec43260000be273d28a5

### Installation ###

* Make sure you have NodeJS installed.
* Go to the project's folder on the terminal.
* Run 
```
#!shell

npm install
```
* Run 
```
#!shell

gulp run
```
* The system will run on **localhost:5000**

* The componente will be rendered from the component tag in the HTML file:

```
#!shell
<mailbox url="'emails'"></mailbox>

```

* The attribute **url** represents the path where the component will be fetching email data.



### Tech stack ###

* AngularJS
* NodeJS / Express
* Lo-Dash
* HTML
* CSS/SASS
* Bootstrap Grid
* Gulp

### Notes ###

* No unit tests included.
* JSON file available on the server side.

### Contact ###

* **Marco Nascimento**
* marco.acrn@gmail.com