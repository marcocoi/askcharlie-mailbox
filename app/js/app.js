(function(){
    'use strict';
    var AppDependencies = [
        'ui.router'
    ];

    angular
        .module('mailApp', AppDependencies)

        .config(AppConfiguration);

    function AppConfiguration($locationProvider, $sceProvider, $stateProvider, $httpProvider) {

        $sceProvider.enabled(false);

        var urlProperties = {enabled: true, requireBase: true};

        $locationProvider.html5Mode(urlProperties).hashPrefix('!');

        var emailViewState = {
            url: 'email/:id',
            templateUrl: 'views/email_view.html',
            controller: 'EmailViewCtrl',
            controllerAs: 'viewCtrl'
        };

        $stateProvider.state('email-view', emailViewState);
    }
})();

