(function () {
    'use strict';

    angular.module('mailApp')


        .service('MailService', function ($http, $q) {
            var allEmails = [];

            return {
                getAllEmails: getAllEmails,
                getEmail: getEmail,
                deleteEmail: deleteEmail
            };

            function getAllEmails(url) {

                var q = $q.defer();

                if (allEmails.length > 0) {

                    return q.resolve(allEmails);
                } else {
                    $http({
                        method: 'GET',
                        url: url
                    }).then(function (response) {
                        allEmails = response.data.messages;
                        q.resolve(allEmails);
                    });

                    return q.promise;
                }
            }
            function getEmail(id) {

                var q = $q.defer();

                if (allEmails.length > 0){
                    _.find(allEmails, function (e) {
                        if (e.uid == id) {
                            q.resolve(e);
                        }
                    });
                } else {
                    q.reject({message:"Invalid email."});
                }
                return q.promise;
            }
            function deleteEmail(id) {
                _.remove(allEmails, {
                    uid: id
                });
                return allEmails;
            }
        });
})();