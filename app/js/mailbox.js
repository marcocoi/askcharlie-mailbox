function MailboxController($scope, $element, $attrs, MailService, $state, $log) {
    var ctrl = this;
    var url = ctrl.url;

    MailService.getAllEmails(url).then(mailsSuccess, mailsError);

    $state.go('email-view');

    ctrl.emails = [];


    ctrl.deleteEmail = function (id) {

        ctrl.emails = MailService.deleteEmail(id);
        $state.go('email-view', {id: ""});
    };

    ctrl.markAsRead = function(email) {
        email.read = true;
    };

    function mailsSuccess(messages) {
        ctrl.emails = messages;
    }
    function mailsError(response) {
        $log.error('An error occurred when trying to fetch inbox data.');
        $log.debug(response);
    }
}

angular.module('mailApp').component('mailbox', {
    bindings: {
        url: '<',
    },
    templateUrl: 'views/mailbox.html',
    controller: MailboxController
});